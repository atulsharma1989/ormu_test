#!/bin/python

### Importing cryptography module to generate encrypted string to use in mysql

from cryptography.fernet import Fernet
key = Fernet.generate_key() #this is your "password"
cipher_suite = Fernet(key)
encoded_text = cipher_suite.encrypt(b"Mysql@12")
decoded_text = cipher_suite.decrypt(encoded_text)

print "Printing encrypted string",encoded_text
print "Printing decrpted value of the encrypted string",decoded_text
