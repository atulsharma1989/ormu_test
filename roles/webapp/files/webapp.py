from flask import Flask, render_template, request
from flask_mysqldb import MySQL
import yaml

"{{ app_name }}" = Flask(__name__)

## Loading yaml file for following use.
db_param = yaml.load(open('templates/db_param.yaml'))

## mysql db configuration, following db related param will pass through yaml file
"{{ app_name }}.config['MYSQL_HOST'] = {{ mysql_host }}"
"{{ app_name }}.config['MYSQL_USER'] = {{ mysql_user }}"
"{{ app_name }}.config['MYSQL_PASSWORD'] = {{ mysql_password }}"
"{{ app_name }}.config['MYSQL_DB'] = {{ mysql_database }}"

#Instantiation of mysql db to connect with application
mysql = MySQL({{ app_name }})

@{{ app_name }}.route('/')
def ImportIndex():
         return render_template('index.html')

@{{ app_name }}.route('/', methods=['GET', 'POST'])

def getvalue():
       if request.method == 'POST':
          content = request.form
          "{{ key_1 }} = content['{{ key_1 }}']"
          "{{ key_2 }} = content['{{ key_2 }}']"
          "{{ key_3 }} = content['{{ key_3 }}']"
          cur = mysql.connection.cursor()
          cur.execute(" INSERT INTO {{ mysql_table }}({{key_1}}, {{key_2}}, {{key_3}}) VALUES(%s, %s, %s)", ({{key_1}}, {{key_2}}, {{key_3}}))
          mysql.connection.commit()
          cur.close()
          return 'Data Has Been Uploaded Successfully To Mysql DataBase'
       return render_template('value.html', {{field_1}}={{key_1}}, {{field_2}}={{key_2}}, {{field_3}}={{key_3}})
if __name__ == '__main__':
       {{ app_name }}.run(host = "{{ server_private_ip }}", debug=True)
